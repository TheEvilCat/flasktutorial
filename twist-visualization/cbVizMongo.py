#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
cbVizMongo.py: Does really cool stuff

Usage:
    cbVizMongo.py [options] [-q | -v]
    cbVizMongo.py -c CONFIG [options] [-q | -v]
    cbVizMongo.py --config

Options:
    -m URI, --mongo URI       MongoDB connection URI [default: mongodb://localhost:27017/]

Other options:
    --config                  prints empty config file
    -c CONFIG                 loads config file
    -h, --help                show this help message and exit
    -q, --quiet               print less text
    -v, --verbose             print more text
    --version                 show version and exit
"""

__author__ = "Mikolaj Chwalisz"
__copyright__ = "Copyright (c) 2013, Technische Universität Berlin"
__version__ = "0.2.0"
__email__ = "chwalisz@tkn.tu-berlin.de"

import logging
import simplejson as json
import sys
from pymongo import MongoClient
import threading
import time

class cbVizMongo(threading.Thread):
    """cbVizMongo description"""
    def __init__(self, uri):
        threading.Thread.__init__(self)
        self.daemon = True
        self.log = logging.getLogger('cbVizMongo')
        self.log.info('cbVizMongo created.')
        self.client = MongoClient(uri)
        self.db = self.client['crewdemo']

    # def __init__

    def close(self):
        self.client.disconnect()
    # def close

    def run(self):
        while True:
            try:
                self.log.debug(self.db.rssi.find_one())
                time.sleep(1)
            except KeyboardInterrupt:
                self.log.debug("KeyboardInterrupt")

    # def run

def main(args):
    """Run the code for cbVizMongo"""
    log = logging.getLogger('cbVizMongo.main')
    log.info(args)
    cbviz = cbVizMongo(args['--mongo'])
    cbviz.start()
    while True:
        try:
            time.sleep(1)
        except KeyboardInterrupt:
            log.debug("KeyboardInterrupt")
            break
        except:
            log.error("Something crashed.")
            cbviz.close()
            break
# def main

def parse_json_config(args):
    """
    Takes care of the correct configure file management.

    It either prints the empty json config structure or adds the
    parameters from the given one to the existing arguments (args)
    """
    if args['--config']:
        del args['-c']; del args['--config']
        del args['--help']; del args['--quiet']
        del args['--verbose']; del args['--version']
        print json.dumps(args, sort_keys=True, indent=4 * ' ')
        sys.exit()
    if args['-c']:
        json_config = json.loads(open(args['-c']).read())
        return dict((str(key), args.get(key) or json_config.get(key))
            for key in set(json_config) | set(args))
    return args
# def parse_json_config

if __name__ == "__main__":
    try:
        from docopt import docopt
    except:
        print """
        Please install docopt using:
            pip install docopt==0.6.1
        For more refer to:
        https://github.com/docopt/docopt
        """
        raise

    args = docopt(__doc__, version=__version__)
    args = parse_json_config(args)

    log_level = logging.INFO  # default
    if args['--verbose']:
        log_level = logging.DEBUG
    elif args['--quiet']:
        log_level = logging.ERROR
    logging.basicConfig(level=log_level,
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    main(args)
