#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
twistvis.py: Flask based webserver for TWIST

Usage:
    twistvis.py [options] [-q | -v]
    twistvis.py -c CONFIG [options] [-q | -v]
    twistvis.py --config

Options:
    -p PORT, --port PORT      port to run flask [default: 5000]
    -m URL, --mongo URL       url to mongodb [default: mongodb://localhost:27017/]

Other options:
    --config                  prints empty config file
    -c CONFIG                 loads config file
    -d, --debug               Start Flask in debug mode
    -h, --help                show this help message and exit
    -q, --quiet               print less text
    -v, --verbose             print more text
    --version                 show version and exit
"""

__author__ = "Mikolaj Chwalisz"
__copyright__ = "Copyright (c) 2013, Technische Universität Berlin"
__version__ = "0.2.0"
__email__ = "chwalisz@tkn.tu-berlin.de"

from gevent import monkey
monkey.patch_all()
import logging
import simplejson as json
import sys
import flask
from flask import render_template
from gevent import pywsgi
import gevent
import geventwebsocket
if geventwebsocket.VERSION < (0, 9, 2, 'final', 0):
    raise ImportError("Required geventwebsocket newer than 0.9.2")
from flask_sockets import Sockets
import time
from pymongo import MongoClient
import pymongo
from bson.json_util import dumps

class ReverseProxied(object):
    '''Wrap the application in this middleware and configure the
    front-end server to add these headers, to let you quietly bind
    this to a URL other than / and to an HTTP scheme that is
    different than what is used locally.

    In nginx:
    location /myprefix {
        proxy_pass http://192.168.0.1:5001;
        proxy_set_header Host $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Scheme $scheme;
        proxy_set_header X-Script-Name /myprefix;
        }

    :param app: the WSGI application
    '''
    def __init__(self, app):
        self.app = app

    def __call__(self, environ, start_response):
        script_name = environ.get('HTTP_X_SCRIPT_NAME', '')
        if script_name:
            environ['SCRIPT_NAME'] = script_name
            path_info = environ['PATH_INFO']
            if path_info.startswith(script_name):
                environ['PATH_INFO'] = path_info[len(script_name):]

        scheme = environ.get('HTTP_X_SCHEME', '')
        if scheme:
            environ['wsgi.url_scheme'] = scheme
        return self.app(environ, start_response)

app = flask.Flask(__name__)
app.wsgi_app = ReverseProxied(app.wsgi_app)
sockets = Sockets(app)
client = None
mongoUrl = None

@app.route('/')
@app.route('/index.html')
def index():
    return render_template("index.html")
# def index

@app.route('/node_map.html')
@app.route('/node_map.html/floor<int:floor>')
def node_map(floor=None):
    if floor == None:
        return render_template("node_map.html", floors=range(2,5))
    elif ( 2 <= floor <= 4):
        return render_template("node_map.html", floors=[floor])
    else:
        flask.abort(404)
# def node_map

@app.route('/spectrum_map.html')
@app.route('/spectrum_map.html/floor<int:floor>')
def spectrum_map(floor=None):
    if floor == None:
        return render_template("spectrum_map.html", floors=range(2,5))
    elif ( 2 <= floor <= 4):
        return render_template("spectrum_map.html", floors=[floor])
    else:
        flask.abort(404)
# def spectrum_map

@app.route('/evarilos_demo.html')
@app.route('/evarilos_demo.html/floor<int:floor>')
def evarilos_demo(floor=None):
    if floor is None:
        return render_template("evarilos_demo.html", floors=range(2,5))
    elif 2 <= floor <= 4:
        return render_template("evarilos_demo.html", floors=[floor])
    else:
        flask.abort(404)
# def evarilos_demo

@sockets.route('/cbtmoterssi')
def cbtmoterssi_socket(ws):
    global client
    log = logging.getLogger('twistvis.cbtmoterssi.sender')
    if client is None or not client.alive():
        if client is not None:
            client.disconnect()
        try:
            client = MongoClient(mongoUrl)
        except pymongo.errors.ConnectionFailure, e:
            return
    crewdb = client['crewdemo']
    dbnodes = crewdb.rssi.distinct("node_id")
    log.debug(dbnodes)
    gevent.spawn(cbtmoterssi_receive,ws)
    try:
        while not ws.closed:
            if not client.alive():
                log.info("mongodb is dead")
                break
            for node in dbnodes:
                rssival = crewdb.rssi.find({"node_id": node}, limit=1,
                    sort=[('TimeStampSec', pymongo.DESCENDING),
                        ('TimeStampMicroSec', pymongo.DESCENDING)])
                ws.send(dumps(rssival[0]))
            time.sleep(1)
    except geventwebsocket.WebSocketError:
        rssival.close()
        log.debug("WebSocketError: Socket closed")

# def cbtmoterssi_socket

def cbtmoterssi_receive(ws):
    log = logging.getLogger('twistvis.cbtmoterssi.receiver')
    while not ws.closed:
        message = ws.receive()
        log.debug(message)
# def cbtmoterssi_receive

def main(args):
    """Run the code for twistvis"""
    log = logging.getLogger('twistvis.main')
    log.debug(args)
    if args['--debug']:
        app.debug = True
    global mongoUrl
    mongoUrl = args['--mongo']
    server = pywsgi.WSGIServer(('', int(args['--port'])), app, handler_class=geventwebsocket.handler.WebSocketHandler)
    server.serve_forever()
# def main

def parse_json_config(args):
    """
    Takes care of the correct configure file management.

    It either prints the empty json config structure or adds the
    parameters from the given one to the existing arguments (args)
    """
    if args['--config']:
        del args['-c']; del args['--config']
        del args['--help']; del args['--quiet']
        del args['--verbose']; del args['--version']
        print json.dumps(args, sort_keys=True, indent=4 * ' ')
        sys.exit()
    if args['-c']:
        json_config = json.loads(open(args['-c']).read())
        return dict((str(key), args.get(key) or json_config.get(key))
            for key in set(json_config) | set(args))
    return args
# def parse_json_config

if __name__ == "__main__":
    try:
        from docopt import docopt
    except:
        print """
        Please install docopt using:
            pip install docopt==0.6.1
        For more refer to:
        https://github.com/docopt/docopt
        """
        raise

    args = docopt(__doc__, version=__version__)
    args = parse_json_config(args)

    log_level = logging.INFO  # default
    if args['--verbose']:
        log_level = logging.DEBUG
    elif args['--quiet']:
        log_level = logging.ERROR
    logging.basicConfig(level=log_level,
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    main(args)
# if __name__
