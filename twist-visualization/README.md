# TWIST Visualizer

This is an graphical user interface for the TWIST testbed nodes.

## Requirements

Please look into `requirements.txt`


## Running from docker

It is possible to run the application through docker container.

In order to get it run:

```bash
docker pull mchwalisz/twist-visualization
```

*Node:* for building and pushing run to the docker hub run:

```bash
docker build -t mchwalisz/twist-visualization .
docker push mchwalisz/twist-visualization
```
