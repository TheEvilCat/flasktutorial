Author: Mikolaj Chwalisz

I tried to synchronize all `.svg` files for all floors.
The standard `.svg` file size should be `700x400 px` big.
The golden point is marked with red dot and is at `(65,65)px`.

Scale:

	1m = 20px
