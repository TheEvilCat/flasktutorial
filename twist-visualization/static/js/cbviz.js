var colors = [
	"rgb(0,0,143)",
	"rgb(0,0,255)",
	"rgb(0,255,255)",
	"rgb(255,255,0)",
	"rgb(255,0,0)",
	"rgb(128,0,0)"
];

var rssiRange = [-100, -45];
var ieee802154 = d3.range(11, 26 + 1);
var colorScale = d3.scale.linear()
	.domain(d3.range(0, 1.1, 1 / 5))
	.range(colorbrewer.YlOrRd[6]);

// dynamic bit...
var colorScaleDynamic = d3.scale.linear().domain(rssiRange).range([0, 1]);

function rssiScale(rssi) {
	return colorScale(colorScaleDynamic(rssi));
};

function plotColorAxis(floor) {
	var svg = d3.select("svg#floor" + floor);
	//Define Color axis
	var data1 = d3.range(rssiRange[0], rssiRange[1], 5);

	var rects = svg.append("g")
		.attr("class", "axis")
		.attr("transform", "translate(" + (twistMap.xgp) + "," + (twistMap.height - twistMap.ygp / 2) + ") scale(0.4)")


	rects.selectAll("rect")
		.data(data1)
		.enter()
		.append("rect")
		.attr({
			width: 40,
			height: 30,
			y: 0,
			x: function(d, i) {
				return i * 45;
			},
			fill: function(d, i) {
				return rssiScale(d);
			},
			opacity: 0.6
		});
	rects.selectAll("text")
		.data(data1)
		.enter()
		.append("text")
		.attr({
			y: 55,
			x: function(d, i) {
				return i * 45 + 20;
			}
		})
		.style("text-anchor", "middle")
		.text(function(d) {
			return d;
		})
	rects.append("text")
		.attr({
			y: 75,
			x: 0
		})
		.text("Power level [dbm]")
};

var oldDataInterval = false;

function toggleOldData() {
	if (oldDataInterval) {
		d3.selectAll("button#toggleOldData").classed("active", false);
		clearInterval(oldDataInterval);
		oldDataInterval = false;
		d3.selectAll("svg")
			.selectAll("g.node")
			.selectAll(".spectrum")
			.classed("olddata", false);
	} else {
		d3.selectAll("button#toggleOldData").classed("active", true);
		fadeOldData();
		oldDataInterval = setInterval(function() {
			fadeOldData();
		}, 5000);
	}
};

function fadeOldData() {
	var tnow = new Date().getTime();
	var svg = d3.selectAll("svg");
	var circles = svg.selectAll("g.node.tmote")
		.selectAll("circle.spectrum")
		.classed("olddata", function(d) {
			if ((d.TimeStampSec * 1000 + 5000) >= tnow) {
				return false;
			} else {
				return true;
			};
		});
};

var cbtmoterssiHost = "ws://" + window.location.hostname + ":" + window.location.port + "/cbtmoterssi";
var cbtmoterssiWs;
// Web socket stuff
function CBtmoteRssiConnect() {
	try {
		cbtmoterssiWs = new WebSocket(cbtmoterssiHost);

		cbtmoterssiWs.onopen = function(e) {
			console.log("Connected to " + cbtmoterssiHost);
		};

		cbtmoterssiWs.onclose = function(e) {
			console.log("Disconnected from " + cbtmoterssiHost);
		};

		cbtmoterssiWs.onmessage = function(e) {
			var msg = jQuery.parseJSON(e.data);
			$.extend(nodes.filter(function(d) {
				return d.node_id == msg.node_id
			})[0], msg)
			var twistnodes = d3.selectAll("g#tmote_" + msg.node_id)
				.selectAll("circle.spectrum")
				.attr("fill", function(D) {
					return rssiScale(D["ch" + channel]);
				});
		};

		cbtmoterssiWs.onerror = function(e) {
			console.log("cbtmoterssi error:", e);
		};
	} catch (ex) {
		console.log("cbtmoterssi exception:", ex);
	}
}

function CBtmoteRssiDisconnect() {
	cbtmoterssiWs.onclose = function() {
		console.log("Disconnected from " + cbtmoterssiHost);
	};
	cbtmoterssiWs.close();
	console.log("Disconnecting from " + cbtmoterssiHost + " ....");
}

function setChannel(ch) {
	$("#setChannel button").removeClass("active")
	$("#setChannel" + ch).addClass("active")
	channel = ch;
	var twistnodes = d3.selectAll("g.node.tmote")
		.selectAll("circle.spectrum")
		.attr("fill", function(D) {
			return rssiScale(D["ch" + channel]);
		});
}

function addSpectrumCircles() {
	var twistnodes = d3.selectAll("g.node.tmote")
	twistnodes
		.insert("circle", ":first-child")
		.attr("class", "spectrum")
		.attr("r", 20);
	addSetChannelLegend();
	d3.selectAll("#options")
		.append("button")
		.attr("id", "toggleOldData")
		.attr("href", "#")
		.attr("class", "btn btn-default")
		.text("Fade out old data")
		.on("click", function(d) {
			toggleOldData();
			event.preventDefault();
			return false;
		});
}

function addSetChannelLegend() {
	var multidim = [];
	while (ieee802154.length) multidim.push(ieee802154.splice(0, 4));
	var legendItem = d3.select("div#setChannel")
		.selectAll("div")
		.data(multidim)
		.enter()
		.append("div")
		.attr("class", "btn-group btn-group-justified")
	legendItem.selectAll("div")
		.data(function(d) {
			return d;
		})
		.enter()
		.append("div")
		.attr("class", "btn-group btn-group-sm")
		.append("button")
		.attr("id", function(d) {
			return "setChannel" + d;
		})
		.text(function(d) {
			return d;
		})
		.attr("class", "btn btn-default")
		.attr("href", "#")
		.attr("title", function(d) {
			return "Switch displayed channel to " + d;
		})
		.on("click", function(d) {
			setChannel(d);
			event.preventDefault();
			return false;
		})
}
