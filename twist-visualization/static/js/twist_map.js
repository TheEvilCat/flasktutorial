var twistMap = {
	//If changed change also svg id="chart" parameters
	width: 700,
	height: 400,
	xgp: 65, // Golden point Xpx
	ygp: 65, // Golden point Xpx
	scale: 20,
	xrange: [0, 32],
	yrange: [0, 16]
};

// Convert meters to pixels
function mToPx(x) {
	return x * twistMap.scale;
};

function pxToM(x) {
	return x / twistMap.scale;
};

var xScale = d3.scale.linear()
	.domain(twistMap.xrange)
	.range([twistMap.xgp, twistMap.xgp + mToPx(twistMap.xrange[1])])
	.clamp(true);

var yScale = d3.scale.linear()
	.domain(twistMap.yrange)
	.rangeRound([twistMap.height - twistMap.ygp,
		twistMap.height - twistMap.ygp - mToPx(twistMap.yrange[1])
	])
	.clamp(true);

function parseNodeData(d) {
	return {
		node_id: d.node_id,
		platform: d.platform,
		x: +d.x,
		y: +d.y,
		z: +d.z,
		room: d.room,
		mac: d.mac,
		ip: d.ip,
		status: d.status
	}
};

function parsePlatformData(d) {
	return {
		full_name: d.full_name,
		name: d.name,
		shape: d.shape,
		shape_size: +d.shape_size,
		exists: false,
		image: d.image
	}
};

function NodeDataUnique(results) {
	var list = [];
	results.forEach(function(d) {
		$.merge(list, d);
	});
	list.sort(function(a, b) {
		if (a.platform > b.platform)
			return 1;
		if (a.platform < b.platform)
			return -1;
		if (a.node_id > b.node_id)
			return 1;
		if (a.node_id < b.node_id)
			return -1;
		return 0;
	});
	for (var i = 1; i < list.length; i) {
		if (list[i - 1].node_id == list[i].node_id) {
			list[i - 1] = $.extend({}, list[i - 1], list[i]);
			list.splice(i, 1);
		} else {
			i++;
		};
	};
	return list;
}

function drawBaseMap(floor) {
	//Set floor parameters:
	switch (floor) {
		case 2:
			var flink = static_url + "/static/data/FT2nd_floorplan.svg";
			var xtext = "FT 2nd Floor, X-Axis [m]"
			break;
		case 3:
			var flink = static_url + "/static/data/FT3rd_floorplan.svg";
			var xtext = "FT 3rd Floor, X-Axis [m]"
			break;
		case 4:
			var flink = static_url + "/static/data/FT4th_floorplan.svg";
			var xtext = "FT 4th Floor, X-Axis [m]"
			break;
		case 100:
			var flink = static_url + "/static/data/FT4th_floorplan.svg";
			var xtext = "FT 4th Floor, X-Axis [m]"
			break;
		default:
			console.error("Wrong floor number");
			return false;
	};
	//Find svg correct svg
	var svg = d3.select("svg#floor" + floor)
	//Draw underlying image:
	d3.html(flink, function(data) {
		xml = d3.select(data)
		icon = document.importNode(xml.select("svg").node(), true)

		svg.insert("g", ":first-child")
			.attr("x", 0)
			.attr("y", 0)
			.attr("width", twistMap.width)
			.attr("height", twistMap.height)
			.attr("opacity", 0.5)
			.attr("id", "floorPlan")
			.node().appendChild(icon);
	});
	//Define X axis
	var xAxis = d3.svg.axis()
		.scale(xScale)
		.orient("bottom");
	//Create X axis
	svg.append("g")
		.attr("class", "axis")
		.attr("transform", "translate(0," + yScale(twistMap.yrange[0]) + ")")
		.call(xAxis);
	//Create X axis label
	svg.append("text")
		.attr("x", twistMap.width / 2 + twistMap.xgp)
		.attr("y", twistMap.height - twistMap.ygp / 2)
		.attr("alignment-baseline", "baseline")
		.style("text-anchor", "middle")
		.text(xtext);
	//Define Y axis
	var yAxis = d3.svg.axis()
		.scale(yScale)
		.orient("left")
		.ticks(5);
	//Create Y axis
	svg.append("g")
		.attr("class", "axis")
		.attr("transform", "translate(" + xScale(twistMap.xrange[0]) + ",0)")
		.call(yAxis);
	//Create Y axis label
	svg.append("text")
		.style("text-anchor", "end")
		.attr("dy", "-.35em")
		.attr("transform",
			"translate(" + (twistMap.xgp / 2) + "," + (twistMap.height - twistMap.height / 2 - twistMap.ygp) + ")" +
			"rotate(-90)")
		.text("Y-Axis [m]");

	//Dynamic resize the whole svg image
	var chart = $("svg#floor" + floor),
		aspect = chart.width() / chart.height(),
		container = chart.parent();
	$(window).on("resize", function() {
		var targetWidth = container.width();
		chart.attr("width", targetWidth);
		chart.attr("height", Math.round(targetWidth / aspect));
	}).trigger("resize");

	$('button').tooltip();

	d3.selectAll("#options")
		.append("button")
		.attr("id", "svgdataurl" + floor)
		.attr("href", "#")
		.attr("class", "btn btn-default disabled")
		.text("Save " + xtext.split(",")[0])
		.on("click", function(d) {
			svgAsDataUri($("svg#floor" + floor)[0], 1, function(uri) {
				var pom = document.createElement('a');
				pom.href = uri;
				pom.download = "imagedata.svg";
				pom.click();
			});
		});
};


function plotAllNodes(floor) {
	//Find svg correct svg
	var svg = d3.select("svg#floor" + floor)
	//Add group for each node
	var svgnodes = svg.selectAll("#node")
		.data(nodes, function(d) {
			return d.node_id;
		})
		.enter()
		.append("g")
		.filter(function(d) {
			var matcher = new RegExp("FT" + floor + ".*", "g");
			return matcher.test(d.room)
		})
		.attr("class", function(d) {
			for (var i = platforms.length - 1; i >= 0; i--) {
				if (platforms[i].name == d.platform) {
					platforms[i].exists = true;
					break;
				}
			};
			return "node " + d.platform;
		})
		.attr("id", function(d) {
			return d.platform + "_" + d.node_id;
		})
		.attr("transform", function(d) {
			return "translate(" + xScale(d.x) + "," + yScale(d.y) + ")";
		})

	//Define tooltip
	var tip = d3.tip()
		.attr('class', 'd3-tip tooltip panel panel-info')
		.direction(function(d) {
			if (d3.event.pageX > (window.innerWidth / 2)) {
				if (d3.event.pageY > (window.innerHeight / 2)) {
					return 'nw';
				} else {
					return 'sw';
				};
			} else {
				if (d3.event.pageY > (window.innerHeight / 2)) {
					return 'ne';
				} else {
					return 'se';
				};
			};
		})
		.html(function(d) {
			function def(txt, val) {
				return "<b>" + txt + ":</b> " + val + "</br>";
			}
			var tiptxt = "<div class='panel-body'>"
			tiptxt += def("Node", d.node_id);
			tiptxt += def("Platform", d.platform);
			tiptxt += def("Room", d.room);
			tiptxt += def("Position (x,y,z)", d.x + ", " + d.y + ", " + d.z);
			if (typeof d.ip !== "undefined") {
				tiptxt += def("IP", d.ip);
			};
			if (typeof d.mac !== "undefined") {
				tiptxt += def("MAC", d.mac);
			}
			if (['tplink', 'twistbot', 'alixap'].indexOf(d.platform) >= 0) {
				tiptxt += def("Load graph", "<img src='http://crewserver1/cgp/graph.php?" +
					"p=load&c=&pi=&t=load&s=86400" +
					"&h=" + d.node_id + "' class='img-responsive'>");
			}
			tiptxt += "</div>"
			return tiptxt;
		})
	svg.call(tip)

	//Add tooltip actions
	svgnodes
		.on("mouseenter", function(d) {
			tip.show(d)
		})
		.on("mouseleave", function(d) {
			tip.hide();
		});

	//Add base element for each node
	svgnodes.append("path")
		.attr("fill", function(d) {
			return "url(#grad" + d.platform + ")";
		})
		.attr("class", "element")
		.style("opacity",function(d) {
			if (d.status == "off") {
				return 0.2;
			} else {
				return 1;
			}
		})
		.attr("d", d3.superformula()
			.type(function(d) {
				return platformParameters(d.platform).shape;
			})
			.size(function(d) {
				return platformParameters(d.platform).shape_size;
			})
			.segments(360)
	);

	//Add optional image for each node
	svgnodes.append("svg:image")
		.attr("x", -15)
		.attr("y", -15)
		.attr("width", "30px")
		.attr("height", "30px")
		.attr("class", "disabled")
		.attr("xlink:href", function(d) {
			if (platformParameters(d.platform).image) {
				return static_url + "/static/image/" + platformParameters(d.platform).image;
			}
		});
};

function addLegend() {
	var libtn = d3.select("div#platformLegend")
		.selectAll("button")
		.data(platforms, function(d) {
			return d.name;
		})
		.enter()
		.append("button")
		.attr("href", "#")
		.attr("title", function(d) {
			return "Toggle visibility of " + d.full_name + " on the maps";
		})
		.on("click", function(d) {
			togglePlatform(d.name);
			event.preventDefault();
			return false;
		})
		.attr("class", function(d) {
			return d.name + " btn btn-default active";
		});
	libtn.append("svg")
		.attr("width", "1em").attr("height", "1.1em")
		.append("circle").attr("cx", "0.45em").attr("cy", "0.55em").attr("r", "0.45em")
		.style("fill", function(d) {
			return "url(#grad" + d.name + ")";
		})
		.attr("class", function(d) {
			return d.name;
		})
		.attr("id", function(d) {
			return "legend" + d.name;
		})
	libtn.append("text")
		.text(function(d) {
			return d.full_name;
		});
	libtn.filter(function(d) {
		return !d.exists;
	}).remove();
	buildGradients();
	d3.selectAll("#options")
		.append("button")
		.attr("id", "toggleImages")
		.attr("href", "#")
		.attr("class", "btn btn-default btn-sm")
		.text("Platform pictures")
		.on("click", function(d) {
			toggleImages();
			event.preventDefault();
			return false;
		});
}

function togglePlatform(platform) {
	var svg = d3.selectAll("svg")
	if (platformParameters(platform).disabled == 1) {
		svg.selectAll("." + platform)
			.classed("disabled", false)
		d3.selectAll("button." + platform).classed("active", true);
		platformParameters(platform).disabled = 0;
		if (platform == "twistbot" && typeof twistBotConnect === 'function') {
			twistBotConnect()
		}
		if (platform == "tmote" && typeof CBtmoteRssiConnect === 'function') {
			CBtmoteRssiConnect()
		}
	} else {
		svg.selectAll("." + platform)
			.classed("disabled", true);
		d3.selectAll("button." + platform).classed("active", false);
		platformParameters(platform).disabled = 1;
		if (platform == "twistbot" && typeof twistBotDisconnect === 'function') {
			twistBotDisconnect()
		}
		if (platform == "tmote" && typeof CBtmoteRssiDisconnect === 'function') {
			CBtmoteRssiDisconnect()
		}
	}
}

function toggleImages() {
	var node = d3.selectAll("svg").selectAll(".node")
	node.selectAll("image")
		.classed("disabled", toggleDisabledClass);
	node.selectAll("path")
		.classed("disabled", toggleDisabledClass);
	d3.selectAll("button#toggleImages").classed("active", toggleDisabledClass);
}

function toggleDisabledClass(d) {
	if (this.getAttribute("class").indexOf("disabled") == -1) {
		return true;
	} else {
		return false;
	};
};

function platformParameters(platform) {
	return $.grep(platforms, function(e) {
		return e.name == platform;
	})[0]
}


function fillGradient(d) {
	return "url(#grad" + d + ")";
}

function hexToR(h) {
	return parseInt((cutHex(h)).substring(0, 2), 16)
}

function hexToG(h) {
	return parseInt((cutHex(h)).substring(2, 4), 16)
}

function hexToB(h) {
	return parseInt((cutHex(h)).substring(4, 6), 16)
}

function cutHex(h) {
	return (h.charAt(0) == "#") ? h.substring(1, 7) : h
}

function scaleColor(d, factor) {
	var scaleR = scaleChannel(hexToR(d), factor);
	var scaleG = scaleChannel(hexToG(d), factor);
	var scaleB = scaleChannel(hexToB(d), factor);
	return ("rgb(" + scaleR + "," + scaleG + "," + scaleB + ")");
}

function scaleChannel(v, factor) {
	return (Math.round(Math.min(v * factor, 255)));
}

function addStop(gradient, offset, color) {
	var stop = gradient.append("stop");
	stop.attr("offset", offset);
	stop.attr("stop-color", color);
}

function buildGradients() {
	var defs = d3.selectAll("svg").append("defs");
	var count = 0
	for (var key in platforms) {
		var d = colorbrewer.Set1[9][count];
		var gradient = defs.append("radialGradient");
		gradient.attr("id", "grad" + platforms[key].name);
		gradient.attr("cx", "50%");
		gradient.attr("cy", "50%");
		gradient.attr("r", "50%");
		addStop(gradient, "0%", scaleColor(d, 1.08));
		addStop(gradient, "33%", scaleColor(d, 1.05));
		addStop(gradient, "66%", scaleColor(d, 0.99));
		addStop(gradient, "85%", scaleColor(d, 0.92));
		addStop(gradient, "100%", scaleColor(d, 0.83));
		count++;
	}
}
