function parseEvarilosNodeData(d) {
	return {
		node_id: d.node_id,
		platform: d.platform,
		room: d.room,
		interferer: $.parseJSON(d.interferer)
	}
};
var interferenceMode = false;

function evarilosInit() {
	var opts = d3.selectAll("#options");
	opts.append("button")
		.attr("id", "#turnInterferers")
		.attr("class", "btn btn-default btn-sm")
		.attr("href", "#")
		.text("Toggle interferers marker")
		.on("click", function(d) {
			if (interferenceMode) {
				turnInterferers(false);
			} else {
				turnInterferers(true);
			};
			event.preventDefault();
			return false;
		});
	opts.append("button")
		.attr("id", "#clearSutMarkers")
		.attr("href", "#")
		.attr("class", "btn btn-default")
		.text("Clear position estimates")
		.on("click", function(d) {
			var node = d3.selectAll("g#sut_sut1")
			node.selectAll("path").remove();
			node.selectAll("circle").remove();
			node.selectAll("line").remove();
			event.preventDefault();
			return false;
		});
	for (var i = platforms.length - 1; i >= 0; i--) {
		if (platforms[i].name == "twistbot" && platforms[i].exists == false) {
			return false; // There are no robots to display
		};
	};
	var robots = $.grep(nodes, function(d) {
		return d.platform == "twistbot"
	})
	robots.forEach(function(tbot) {
		tbot.rosTopic.push(new ROSLIB.Topic({
			ros: tbot.ros,
			name: '/EVARILOS/SUT/estimated_position',
			messageType: 'geometry_msgs/Point'
		}));
		tbot.rosTopic[tbot.rosTopic.length - 1].subscribe(function(message) {
			setSutMarkerPosition(message, tbot.node_id);
		});
		tbot.rosTopic.push(new ROSLIB.Topic({
			ros: tbot.ros,
			name: '/EVARILOS/GUI/localize_event',
			messageType: 'std_msgs/Empty'
		}));
		opts.append("button")
			.attr("id", "#getPositionExtimate")
			.attr("href", "#")
			.attr("class", "btn btn-default")
			.text("Get position of " + tbot.node_id)
			.on("click", function(d) {
				console.log(tbot.node_id + " is trying to get location....");
				var msg = new ROSLIB.Message();
				tbot.rosTopic[tbot.rosTopic.length - 1].publish(msg);
				event.preventDefault();
				return false;
			});
	});
	d3.selectAll("g#sut_sut1")
		.attr("transform", null)
		.selectAll("path").remove();
};

function turnInterferers(mode) {
	interferenceMode = mode;
	var interferers = d3.selectAll("svg").selectAll("g.node");
	var shape = "clover";
	var size = 180;
	interferers.filter(function(d) {
		if (typeof d.interferer !== "undefined" && d.interferer == true) {
			return true;
		} else {
			return false;
		};
	})
		.classed("interferer", mode)
		.selectAll("path")
		.transition().duration(500)
		.attr("d", d3.superformula()
			.type(function(d) {
				if (mode) {
					return shape;
				} else {
					return platformParameters(d.platform).shape;
				}
			})
			.size(function(d) {
				if (mode) {
					return size;
				} else {
					return platformParameters(d.platform).shape_size;
				}
			})
			.segments(360));
};

var setSutMarkerPosition = function(message, robot) {
	console.log("msg")
	$.extend(nodes.filter(function(d) {
		return d.node_id == "sut1"
	})[0], message)
	var tbot = $.grep(nodes, function(d) {
		return d.node_id == robot
	})[0]
	var node = d3.selectAll("g#sut_sut1");
	node.append("line")
		.style("opacity", 1)
		.style("stroke", "gray")
		.attr("x1", function(d) {
			return xScale(d.x);
		})
		.attr("y1", function(d) {
			return yScale(d.y);
		})
		.attr("x2", function(d) {
			return xScale(tbot.x);
		})
		.attr("y2", function(d) {
			return yScale(tbot.y);
		});
	node.append("circle")
		.attr("r", 25)
		.attr("class", "spectrum")
		.style("opacity", 0.2)
		.style("fill", "url(#gradsut)")
		.attr("transform", function(d) {
			return "translate(" + xScale(d.x) + "," + yScale(d.y) + ")";
		})
	node.append("path")
		.classed("sut", true)
		.style("fill", "url(#gradsut)")
		.attr("d", d3.superformula()
			.type(function(d) {
				return platformParameters("sut").shape;
			})
			.size(function(d) {
				return platformParameters("sut").shape_size + 30;
			})
			.segments(360))
		.attr("transform", function(d) {
			return "translate(" + xScale(d.x) + "," + yScale(d.y) + ")";
		})
	node.append("path")
		.classed("robot", true)
		.attr("d", d3.superformula()
			.type(function(d) {
				return platformParameters("twistbot").shape;
			})
			.size(function(d) {
				return platformParameters("twistbot").shape_size + 30;
			})
			.segments(360))
		.attr("transform", function(d) {
			return "translate(" + xScale(tbot.x) + "," + yScale(tbot.y) + ") rotate(" + (tbot.angle) + ")";
		})
		.attr("fill", "gray");
};
