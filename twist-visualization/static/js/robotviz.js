function twistBotConnect() {
	for (var i = platforms.length - 1; i >= 0; i--) {
		if (platforms[i].name == "twistbot" && platforms[i].exists == false) {
			return false; // There are no robots to display
		};
	};
	var robots = $.grep(nodes, function(d) {
		return d.platform == "twistbot"
	})
	robots.forEach(function(tbot) {
		tbot.angle = 0;
		tbot.dragx = 0;
		tbot.dragy = 0;
		tbot.ros = new ROSLIB.Ros();
		tbot.rosWs = "ws://" + tbot.node_id + ".:9090"
		tbot.ros.on('error', function(error) {
			console.log('Connection error on ' + tbot.rosWs);
		})
			.on('connection', function() {
				console.log('Connected to ' + tbot.rosWs);
				d3.selectAll("g#twistbot_" + tbot.node_id)
					.classed("disabled", false);
			})
			.on('close', function() {
				console.log('Disconnected from ' + tbot.rosWs);
				d3.selectAll("g#twistbot_" + tbot.node_id)
					.classed("disabled", true);
			});
		tbot.ros.connect(tbot.rosWs);
		tbot.rosTopic = [new ROSLIB.Topic({
			ros: tbot.ros,
			name: '/absolute_position',
			messageType: 'twistbot_robot/PositionAngle'
		})];
		tbot.rosTopic[tbot.rosTopic.length - 1].subscribe(function(message) {
			moveBaseFeedback(message, tbot.node_id);
		});
	});
	var drag = d3.behavior.drag()
		.origin(Object)
		.on("dragstart", robotDragStart)
		.on("drag", robotDrag)
		.on("dragend", robotDragEnd);
	d3.selectAll("g.twistbot")
		.selectAll("path")
		.call(drag);
};

function robotDragStart(d) {
	d3.event.sourceEvent.stopPropagation();
	d3.event.sourceEvent.preventDefault();
};

function robotDrag(d) {
	d3.event.sourceEvent.stopPropagation();
	d3.event.sourceEvent.preventDefault();
	d.dragx += d3.event.dx;
	d.dragy += d3.event.dy;
	d3.select(this)
		.attr("transform", function(d) {
			return "translate(" + d.dragx + "," + d.dragy + ")" + "rotate(" + d.angle + ")";
		});
};

function robotDragEnd(d) {
	d3.event.sourceEvent.stopPropagation();
	d3.event.sourceEvent.preventDefault();
	var goalx = xScale.invert(xScale(d.x) + d.dragx);
	var goaly = yScale.invert(yScale(d.y) + d.dragy);
	var goal = new ROSLIB.Topic({
		ros: d.ros,
		name: '/move_base_simple/goal',
		messageType: 'geometry_msgs/PoseStamped'
	});
	var msg = new ROSLIB.Message({
		header: {
			frame_id: "/map"
		},
		pose: {
			position: {
				x: goalx,
				y: goaly,
				z: 0
			},
			orientation: {
				x: 0,
				y: 0,
				z: 0,
				w: 1
			}
		}
	});
	goal.publish(msg);
	d.dragx = 0;
	d.dragy = 0;
	// Return robot to a normal position
	d3.select(this).transition()
		.duration(200)
		.attr("transform", function(d) {
			return "translate(" + d.dragx + "," + d.dragy + ")" + "rotate(" + (d.angle) + ")";
		});
	// Add ghost to show where it should go (will disappear after some time)
	d3.select(this.parentNode.parentNode)
		.append("path")
		.attr("d", d3.superformula()
			.type(function(D) {
				return platformParameters(d.platform).shape;
			})
			.size(function(D) {
				return platformParameters(d.platform).shape_size;
			})
			.segments(360)
	)
		.attr("transform", function(D) {
			return "translate(" + xScale(goalx) + "," + yScale(goaly) + ")" + "rotate(" + (d.angle) + ")";
		})
		.style("fill", function(D) {
			return "url(#grad" + d.platform + ")";
		})
		.style("opacity", 0.6)
		.transition()
		.duration(2000)
		.delay(1000)
		.style("opacity", 0)
		.delay(3000)
		.remove()
}

var moveBaseFeedback = function(message, robot) {
	var msg = {
		x: +message.position.x.toFixed(3),
		y: +message.position.y.toFixed(3),
		z: +message.position.z.toFixed(3),
		angle: 180 - message.angle.data * (180 / Math.PI)
	};
	$.extend(nodes.filter(function(d) {
		return d.node_id == robot
	})[0], msg)
	var node = d3.selectAll("g#twistbot_" + robot);
	node.transition()
		.duration(10)
		.attr("transform", function(d) {
			return "translate(" + xScale(d.x) + "," + yScale(d.y) + ")";
		})
	node.selectAll(".element")
		.attr("transform", function(d) {
			return "translate(" + d.dragx + "," + d.dragy + ")" + "rotate(" + (d.angle) + ")";
		})
};

function twistBotDisconnect() {
	var robots = $.grep(nodes, function(d) {
		return d.platform == "twistbot"
	})
	robots.forEach(function(tbot) {
		tbot.rosTopic.forEach(function(topic) {
			topic.unsubscribe();
		});
		tbot.ros.close();
	});
};
