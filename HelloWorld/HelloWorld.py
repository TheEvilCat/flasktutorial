from flask import Flask #Load the Flask module into your Python script
app = Flask(__name__) #Create a Flask object called app

@app.route("/index") #Run the code below this function when someone accesses the root URL of the server

def index(): # this is a view.
    return "Hello World!"

if __name__ == "__main__": #If this script was run directly from the command line
    app.run()
